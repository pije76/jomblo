from django.shortcuts import render_to_response, render
from django.conf import settings
from django.template import RequestContext
from django.http import HttpResponseRedirect,HttpResponse

import ephem
from datetime import datetime, timedelta
from pygooglechart import SimpleLineChart, Chart, Axis

def show_index(request):
    return render(
        request, 'astroephem/show_index.html',
        {
        }
    )

def show_object(request, object):
        """
        Show object page
        """
        now = datetime.now()
        # use ephem planet/moon per given object slug
        if object == 'merkury':
            object = ephem.Mercury()
            name = 'Merkury'
        elif object == 'wenus':
            object = ephem.Venus()
            name = 'Wenus'
        elif object == 'mars':
            object = ephem.Mars()
            name = 'Mars'
        elif object == 'jowisz':
            object = ephem.Jupiter()
            name = 'Jowisz'
        elif object == 'saturn':
            object = ephem.Saturn()
            name = 'Saturn'
        elif object == 'uran':
            object = ephem.Uranus()
            name = 'Uran'
        elif object == 'neptun':
            object = ephem.Neptune()
            name = 'Neptun'
        elif object == 'pluton':
            object = ephem.Pluto()
            name = 'Pluton'
        elif object == 'moon':
            object = ephem.Moon()
            name = u'Księżyc'

        # Polskie tłumaczenia konpagestelacji
        const = {
            'Aries': 'Baran',
            'Taurus': 'Byk',
            'Gemini': 'Bliźnięta',
            'Cancer': 'Rak',
            'Leo': 'Lew',
            'Virgo': 'Panna',
            'Libra': 'Waga',
            'Scorpius': 'Skorpion',
            'Ophiuchus': 'Wężownik',
            'Sagittarius': 'Strzelec',
            'Capricornus': 'Koziorożec',
            'Aquarius': 'Wodnik',
            'Pisces': 'Ryby',
        }

        # calculate the data for Warsaw
        city = ephem.Observer()
        city.lat = '52.15'
        city.long = '21.01'
        city.elevation = 115
        object.compute(city)

        # we collect data for the current date / time
        constellation = ephem.constellation(object)[1]
        if constellation in const.keys():
            constellation = const[constellation]
        data = {'name': name, 'rising': city.next_rising(object), 'transit': city.next_transit(object),
                'setting': city.next_setting(object), 'az': object.az, 'alt': object.alt, 'mag': object.mag,
                'constellation': constellation, 'ra': object.ra, 'dec': object.dec,
                'current_date': now, 'size': object.size, 'distance': object.earth_distance }

        # size and brightness in the next days / months
        days = []
        size = []
        mag = []

        future_object = object
        future_city = city
        future = now
        max_size = 0
        min_size = 0
        max_mag = 0
        min_mag = 0
        for i in range(1,20):
                d = list(future.timetuple())
                future_city.date = '%s/%s/%s' % (d[0], d[1], d[2])
                future_object.compute(future_city)
                if i%3 == 0:
                    days.append(str(future_city.date).split(' ')[0])
                size.append(float(str(future_object.size)[:4]))
                mag.append(future_object.mag)

                if max_size < future_object.size:
                    max_size = future_object.size
                if max_mag < future_object.mag:
                    max_mag = future_object.mag

                if min_mag > future_object.mag:
                    min_mag = future_object.mag
                if min_size > future_object.size:
                    min_size = future_object.size

                future = future + timedelta(days=5)

        # wykres rozmiaru
        chart = SimpleLineChart(670, 250, y_range=[min_size-1, max_size+1])
        chart.add_data(size)
        chart.set_colours(['0000FF'])
        chart.fill_linear_stripes(Chart.CHART, 0, 'CCCCCC', 0.2, 'FFFFFF', 0.2)
        chart.set_grid(0, 25, 5, 5)
        size.reverse()
        chart.set_axis_labels(Axis.LEFT, size)
        dlabels = chart.set_axis_labels(Axis.BOTTOM, days)
        chart.set_axis_positions(dlabels, [5])
        index = chart.set_axis_labels(Axis.TOP, ['Rozmiar %s (arcsec)' % name])
        chart.set_axis_positions(index, [50])
        chart.set_axis_style(index, '202020', font_size=14, alignment=0)
        size_chart = chart.get_url()

        # chart brightness
        chart = SimpleLineChart(670, 250, y_range=[min_mag-1, max_mag+1])
        chart.add_data(mag)
        chart.set_colours(['0000FF'])
        chart.fill_linear_stripes(Chart.CHART, 0, 'CCCCCC', 0.2, 'FFFFFF', 0.2)
        chart.set_grid(0, 25, 5, 5)
        mag.reverse()
        chart.set_axis_labels(Axis.LEFT, mag)
        dlabels = chart.set_axis_labels(Axis.BOTTOM, days)
        chart.set_axis_positions(dlabels, [5])
        index = chart.set_axis_labels(Axis.TOP, ['Jasność %s (mag)' % name])
        chart.set_axis_positions(index, [50])
        chart.set_axis_style(index, '202020', font_size=14, alignment=0)
        mag_chart = chart.get_url()

        # koniec
        return render(
            request, 'astroephem/show_object.html',
            {
                'data': data, 'size_chart': size_chart, 'mag_chart': mag_chart
            }
        )
