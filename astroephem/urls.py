from django.conf.urls import url, include
from .views import show_object, show_index

urlpatterns = [
    url(r'^obj/(?P<object>[\w\-_]+)/$', show_object, name='show_object'),
    url(r'^$', show_index, name='show_index'),
]
