from django.apps import AppConfig


class AstroephemConfig(AppConfig):
    name = 'astroephem'
