"""
WSGI config for jomblo project.

It exposes the WSGI callable as a module-level variable named ``application``.

For more information on this file, see
https://docs.djangoproject.com/en/1.11/howto/deployment/wsgi/
"""

import os
#import sys
#import site

from django.core.wsgi import get_wsgi_application

#site.addsitedir('~/.virtualenvs/jomblo/lib/python3.5/site-packages')
#sys.path.append('/var/www/html/jomblo')

#sys.path.append('/var/www/html/jomblo/jomblo')
#sys.path.append('~/.virtualenvs/jomblo/lib/python3.5/site-packages')

os.environ.setdefault("DJANGO_SETTINGS_MODULE", "jomblo.settings")

#activate_env=os.path.expanduser("~/.virtualenvs/jomblo/bin/activate_this.py")
#exec(compile(open(activate_env).read(), activate_env, 'exec'), dict(__file__=activate_env))

application = get_wsgi_application()
