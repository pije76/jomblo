from django.conf.urls import url, include
from django.contrib import admin
from django.conf import settings
from django.views.generic import RedirectView
from django_filters.views import FilterView
from django.conf.urls.static import static

from ajax_select import urls as ajax_select_urls

from apps.accounts.views import homepage, ProfileList, ProfileDetail, profiledetail, profileupdate, searchresult, searchform, signupform
from apps.accounts.filters import ProfileFilter


urlpatterns = [
    url(r'^$', homepage, name='homepage'),

#    url(r'^accounts/$', RedirectView.as_view(pattern_name='profilelist', permanent=False)),
    url(r'^profile/$', ProfileList.as_view(), name='profilelist'),
#    url(r'^profile/$', IndexView.as_view(), name="profilelist"),
#    url(r'^profile/(?P<slug>[-\w\-]+)/$', profiledetail, name='profiledetail'),
    url(r'^profile/(?P<slug>[\w.@+-]+)/$', profiledetail, name='profiledetail'),
#    url(r'^profile/(?P<slug>[-\w.]+)/$', ProfileDetail.as_view(), name='profiledetail'),
#    url(r'^profile/(?P<slug>[-\w\-]+)/$', PostCountHitDetailView.as_view(), name="profiledetail"),
#    url(r'^profile/(?P<pk>\d+)/$', PostCountHitDetailView.as_view(), name="profiledetail"),
#    url(r'hitcount/', include('hitcount.urls', namespace='hitcount')),
    url(r'^profile/edit/(?P<profile_id>\d+)/$', profileupdate, name='profileupdate'),

    url(r'^find/$', searchform, name='searchform'),
#    url(r'^find/$', FilterView.as_view(filterset_class=ProfileFilter, template_name='search/search.html'), name='searchform'),
#    url(r'^find/$', Autocomplete.as_view(), name='searchform'),
#    url(r'^find/', include(ajax_select_urls)),
    url(r'^search/$', searchresult, name='searchresult'),

    url(r'^accounts/', include('allauth.urls')),
    url(r'^ephem/', include('astroephem.urls')),
#    url(r'^accounts/signup/', include(ajax_select_urls)),
#    url(r'^accounts/signup/$', signupform, name='signupform'),
#    url(r'^login/$', login, name='login'),
#    url(r'^logout/$', logout, name='logout'),
#    url(r'^password_reset/$', password_reset, name='password_reset'),
#    url(r'^password_reset/done/$', password_reset_done, name='password_reset_done'),
#    url(r'^reset/(?P<uidb64>[0-9A-Za-z_\-]+)/(?P<token>[0-9A-Za-z]{1,13}-[0-9A-Za-z]{1,20})/$', password_reset_confirm, name='password_reset_confirm'),
#    url(r'^reset/done/$', password_reset_complete, name='password_reset_complete'),

#    url(r'^profile/$', profile, name='profile'),
#    url(r'^dashboard/', frontend, name='frontend'),
#    url(r'^profile/', include('apps.accounts.urls', namespace='profile')),
#    url(r'^message/', include('apps.message.urls', namespace='message')),
#    url(r'^grappelli/', include('grappelli.urls')),
    url(r'^admin/lookups/', include(ajax_select_urls)),
    url(r'^admin/', admin.site.urls),
]

if settings.DEBUG:
    import debug_toolbar
    urlpatterns = [
        url(r'^__debug__/', include(debug_toolbar.urls)),
    ] + urlpatterns

if 'rosetta' in settings.INSTALLED_APPS:
    urlpatterns = [
        url(r'^rosetta/', include('rosetta.urls')),
    ] + urlpatterns

if settings.DEBUG:
    urlpatterns += static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
    urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
