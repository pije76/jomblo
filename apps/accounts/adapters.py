from allauth.account.adapter import DefaultAccountAdapter

class AccountAdapter(DefaultAccountAdapter):
    def save_user(self, request, user, form, commit=False):
        data = form.cleaned_data
        user.first_name = data.get('first_name')
        user.gender = data.get('gender')
        user.relationship = data.get('relationship')
        user.birthdate = data.get('birthdate')
        user.birthtime = data.get('birthtime')
        user.birthcity = data.get('birthcity')
#        user.location = data.get('location')

        user.username = data.get('username')
        user.email = data.get('email')
        if 'password1' in data:
            user.set_password(data["password1"])
        else:
            user.set_unusable_password()
        self.populate_username(request, user)
        if commit:
            user.save()
        return user
