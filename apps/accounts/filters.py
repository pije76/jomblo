import django_filters
import ajax_select
#from dal import autocomplete
#from ajax_select.fields import AutoCompleteSelectField

from apps.accounts.models import Profile
#from apps.geonames.models import City


class ProfileFilter(django_filters.FilterSet):
    class Meta:
#        city = AutoCompleteSelectField('city', required=True, help_text='icontains')
#        city = django_filters.CharFilter(name='city', lookup_expr='icontains')
        city = django_filters.CharFilter(name='city', lookup_expr='icontains', widget=ajax_select.fields.AutoCompleteSelectField('city'))
#        username = django_filters.CharFilter(name='username', lookup_expr='icontains', widget=autocomplete.ModelSelect2(url='searchform'))
#        city = django_filters.ModelChoiceFilter(queryset=Profile.objects.all(),widget=autocomplete_light.ChoiceWidget('ProfileAutocomplete'))
#        first_name = django_filters.CharFilter(lookup_expr='icontains')
#        year_joined = django_filters.NumberFilter(name='date_joined', lookup_expr='year')
#        groups = django_filters.ModelMultipleChoiceFilter(queryset=Profile.objects.all(),widget=forms.CheckboxSelectMultiple)

        model = Profile
#        fields = [
#            'username',
#            'city',
#        ]
        fields = {
            'username': ['icontains', ],
#            'city': ['exact', ],
#            'first_name': ['icontains', ],
#            'last_name': ['exact', ],
#            'date_joined': ['year', 'year__gt', 'year__lt', ],
#            'gender': ['exact',],
#            'relationship': ['exact',],
        }
