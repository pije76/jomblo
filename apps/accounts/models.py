from django.contrib.auth.models import (AbstractUser)
from django.core.validators import RegexValidator
from django.db import models
from django.db.models import permalink
from django.db.models.signals import post_save
from django.template.defaultfilters import slugify
from django.utils import timezone
from django.utils.timezone import get_current_timezone, make_aware, utc
from django.contrib.contenttypes.fields import GenericRelation

import datetime
#import ephem
import pytz
import arrow

from autoslug import AutoSlugField
#from django_autoslug.fields import AutoSlugField
from hitcount.models import HitCount, HitCountMixin
from imagekit.models import ImageSpecField
from imagekit.processors import SmartResize

from apps.flatlib import const, aspects
from apps.flatlib.chart import Chart
from apps.flatlib.datetime import Datetime
from apps.flatlib.geopos import GeoPos

from apps.geonames.models import City

GENDER_CHOICES = (
    ('male', 'Male'),
    ('female', 'Female'),
)

RELATIONSHIP_CHOICES = (
    ('single', 'Single'),
    ('widow', 'Widow/Widower'),
    ('divorced', 'Divorced'),
)

RELIGION_CHOICES = (
        ('cristian','Cristian'),
        ('islam','Islam'),
        ('hindu','Hindu'),
        ('buddhist','Buddhist')
    )

EDUCATION_CHOICES = (
        ('smp','Junior High School'),
        ('sma','Senior High School'),
        ('s1','S1'),
        ('s2','S2'),
        ('s3','S3')
    )

# Create your models here.
def localize_datetime(dtime):
    tz_aware = make_aware(dtime, utc).astimezone(get_current_timezone())
    return datetime.datetime.strftime(tz_aware, '%Y-%m-%d %H:%M:%S')


def make_utc(dt):
    if dt.is_naive():
        dt = timezone.make_aware(dt)
        return dt.astimezone(pytz.utc)

def upload_location(instance, filename):
    return "%s/%s" % (instance.slug, filename)


#class Profile(models.Model):
class Profile(AbstractUser, HitCountMixin):
#class Profile(AbstractBaseUser, PermissionsMixin):
#    id = models.AutoField(primary_key=True)
#    email = models.EmailField(verbose_name='email address', blank=False, unique=True)
#    username = models.CharField(verbose_name='username', max_length=100, blank=False, unique=True)
#    first_name = models.CharField(verbose_name='first name', max_length=100, blank=True)
#    last_name = models.CharField(verbose_name='last name', max_length=100, blank=True)
#    date_joined = models.DateTimeField(verbose_name='date joined', auto_now_add=True)
#    is_active = models.BooleanField(verbose_name='active status', default=True)
#    is_staff = models.BooleanField(verbose_name='staff status', default=False)
#    is_admin = models.BooleanField(verbose_name='admin status', default=False)
#    email_confirmed = models.BooleanField(default=False)

    slug = AutoSlugField(populate_from='username', always_update=True, unique=True, unique_with=('date_joined'), null=True, blank=True)
#    slug = AutoSlugField(populate_from=('username',), recursive='city', prefix_from=('id',), unique=True, max_length=255, overwrite=True)
    gender = models.CharField(max_length=100, choices=GENDER_CHOICES, blank=False)
    relationship = models.CharField(max_length=100, choices=RELATIONSHIP_CHOICES, blank=False)
    religion = models.CharField(max_length=50, choices=RELIGION_CHOICES)
    education = models.CharField(max_length=50, choices=EDUCATION_CHOICES)
    workplace = models.CharField(max_length=255)
    annual_income = models.CharField(max_length=20, default="not specified")
    image = models.ImageField(upload_to= upload_location,null=True, blank=True,default="/media/default/pimage.png")
#    profilepicture = models.ImageField(upload_to="profilepictures", null=True, blank=True, default="/media/default/pimage.png")
    thumbnail = ImageSpecField(source='image', processors=[SmartResize(600,600)],format='JPEG', options={'quality':70},)
#    thumbnail = ProcessedImageField(upload_to='thumbnails', processors=[SmartResize(600, 600)], format='JPEG', options={'quality': 60})
    address = models.CharField(verbose_name='Address', max_length=255, blank=False)
#    location = PlainLocationField(based_fields=['address'], zoom=7)
    city = models.ForeignKey(City, on_delete=models.CASCADE, verbose_name='City', related_name="city_set", max_length=100, blank=False, null=True)
#    state = models.CharField(verbose_name='State', max_length=100, blank=True)
    country = models.CharField(verbose_name='Country', max_length=100, blank=False)
    postalcode = models.CharField(verbose_name='Postal Code', max_length=100, blank=True)
    phone_regex = RegexValidator(regex=r'^\+?1?\d{9,15}$', message="Phone number must be entered in the format: '+999999999'. Up to 15 digits allowed.")
    phone_number = models.CharField(max_length=15, validators=[phone_regex], blank=True)
    birthdate = models.DateField(verbose_name='Date of Birth', blank=False, null=True)
    birthtime = models.TimeField(verbose_name='Time of Birth', blank=False, null=True)
    age = models.PositiveIntegerField(default=0, blank=True)
    zodiac = models.CharField(max_length=55, blank=True)
    chiron = models.CharField(max_length=55, blank=True)
    venus = models.CharField(max_length=55, blank=True)
    birthplace = models.ForeignKey(City, on_delete=models.CASCADE, verbose_name='Place of Birth', related_name="birthplace_set", max_length=100, blank=False, null=True)
#    location = gis_models.PointField(u"longitude/latitude", geography=True, blank=False, null=True)
#    location = gis_models.PointField(geography=True, srid=4326, blank=False, null=True)
#    location = PlainLocationField(based_fields=['birthplace'], zoom=7, default=Point(1.0, 1.0))
#    location = GeopositionField(verbose_name='Geoposition', default=None)
#    location = PlacesField(verbose_name='Geoposition', default=None, max_length=255)
    latitude = models.CharField(verbose_name='Latitude', max_length=100, blank=False)
    longitude = models.CharField(verbose_name='Longitude', max_length=100, blank=False)
#    timezone = forms.ChoiceField(label=_('Time Zone'), choices=[(t, t) for t in pytz.common_timezones])
#    zone = models.ForeignKey(City, to_field=timezone, related_name='+', verbose_name='Timezone', max_length=100, blank=True, null=True)
    gmtoffset = models.CharField(verbose_name='GMT Offset', max_length=100, null=True)
    hitcount = GenericRelation(HitCount, object_id_field='object_pk', related_query_name='hitcount_relation')

#    gis = gis_models.GeoManager()
#    objects = gis_models.GeoManager()
#    objects = models.Manager()
#    objects = UserManager()

#    USERNAME_FIELD = 'email'
#    REQUIRED_FIELDS = []

    class Meta:
        db_table = 'profile'
        verbose_name = "Profile"
        verbose_name_plural = "Profiles"

    def __str__(self):              # __unicode__ on Python 2
        return "%s" % self.username
#        return self.email
#        return '%s %s' % (self.first_name, self.last_name)

#    def get_full_name(self):
#        full_name = '%s %s' % (self.first_name, self.last_name)
#        return full_name.strip()

#    def get_short_name(self):
#        return self.username

#    @receiver(post_save, sender=User)
#    def create_user_profile(sender, instance, created, **kwargs):
#        if created:
#            Profile.objects.create(user=instance)

#    @receiver(post_save, sender=User)
#    def update_user_profile(sender, instance, created, **kwargs):
#        if created:
#            Profile.objects.create(user=instance)
#        instance.profile.save()

#    @receiver(post_save, sender=User)
#    def save_user_profile(sender, instance, **kwargs):
#        instance.profile.save()

    @permalink
    def get_absolute_url(self):
        return ('accounts.views.profiledetail', None, {'slug':self.slug})

#    def country(self):
#        if not self.city:
#            return False
#        else:
#            return self.city.country.name

#    def latitude(self):
#        if not self.birthplace:
#            return False
#        else:
#            return self.birthplace.latitude

#    def longitude(self):
#        if not self.birthplace:
#            return False
#        else:
#            return self.birthplace.longitude

#    def gmtoffset(self):
#        if not self.birthplace:
#            return False
#        else:
#            return self.birthplace.gmt_offset

#    def age(self):
#        if not self.birthdate:
#            return False
#        else:
#            today = timezone.now().date()
#            try:
#                birthday = self.birthdate.replace(year=today.year)
#            except ValueError:
#                day = today.day - 1 if today.day != 1 else today.day + 2
#                birthday = self.birthdate.replace(year=today.year, day=day)
#            if birthday > today:
#                return today.year - self.birthdate.year - 1
#            else:
#                return today.year - self.birthdate.year

#    def zodiac(self):
#        if not self.birthtime:
#            return False
#        else:
##            sun = ephem.Sun()
##            sun.compute(self.birthdate)
##            return ephem.constellation(sun)[-1]

#            tgldt = self.birthdate
#            tgl = arrow.get(tgldt).format('YYYY/MM/DD')
#            wkt = self.birthtime
#            waktu = wkt.strftime('%H:%M')
#            gmt = self.birthplace.gmt_offset
#            tglwkt = Datetime(tgl, waktu, gmt)
#            latitude = self.birthplace.latitude
#            longitude = self.birthplace.longitude
#            pos = GeoPos(latitude, longitude)

#            chart = Chart(tglwkt, pos)
#            sun = chart.get(const.SUN)
#            return sun.sign

#    def chiron(self):
#        tgldt = self.birthdate
#        tgl = arrow.get(tgldt).format('YYYY/MM/DD')
#        wkt = self.birthtime
#        waktu = wkt.strftime('%H:%M')
#        gmt = self.birthplace.gmt_offset
#        tglwkt = Datetime(tgl, waktu, gmt)
#        latitude = self.birthplace.latitude
#        longitude = self.birthplace.longitude
#        pos = GeoPos(latitude, longitude)

#        chart = Chart(tglwkt, pos)
#        chiron = chart.get(const.CHIRON)
#        return chiron.sign

#    def venus(self):
#        tgldt = self.birthdate
#        tgl = arrow.get(tgldt).format('YYYY/MM/DD')
#        wkt = self.birthtime
#        waktu = wkt.strftime('%H:%M')
#        gmt = self.birthplace.gmt_offset
#        tglwkt = Datetime(tgl, waktu, gmt)
#        latitude = self.birthplace.latitude
#        longitude = self.birthplace.longitude
#        pos = GeoPos(latitude, longitude)

#        chart = Chart(tglwkt, pos)
#        venus = chart.get(const.VENUS)
#        return venus.sign

#    def aspect(self):
#        tgldt = self.birthdate
#        tgl = arrow.get(tgldt).format('YYYY/MM/DD')
#        wkt = self.birthtime
#        waktu = wkt.strftime('%H:%M')
#        gmt = self.birthplace.gmt_offset
#        tglwkt = Datetime(tgl, waktu, gmt)
#        latitude = self.birthplace.latitude
#        longitude = self.birthplace.longitude
#        pos = GeoPos(latitude, longitude)

#        chart = Chart(tglwkt, pos)
#        chiron = chart.get(const.CHIRON)
#        venus = chart.get(const.VENUS)
#        aspect = aspects.aspectType(chiron, venus, const.MAJOR_ASPECTS)
#        return aspect

#    def last_seen(self):
#        return cache.get('seen_%s' % self.user.user)

#    def online(self):
#        if self.last_seen():
#            now = datetime.datetime.now()
#            if now > self.last_seen() + datetime.timedelta(seconds=settings.USER_ONLINE_TIMEOUT):
#                return False
#            else:
#                return True
#        else:
#            return False

#    def set_coords(self):
#        gmaps = GoogleMaps(api_key)
#        toFind = self.address + ', ' + self.city + ', ' + self.state + ', ' + self.postalcode
#        (place, location) = gmaps.geocode(toFind)

#        self.lat = location[0]
#        self.lng = location[1]

#    def set_coords(sender, **kw):
#        model_instance = kw["instance"]
#        if kw["created"]:
#            toFind = model_instance.address + ', ' + model_instance.city + ', ' + model_instance.province + ', ' + model_instance.postal
#            (place, location) = g.geocode(toFind)
#            model_instance.lat = location[0]
#            model_instance.lng = location[1]
#            model_instance.save()
#    post_save.connect(set_coords, sender=MyModel)

    def user_post_save(sender, instance, created, **kwargs):
        if created == True:
            p = Profile()
            p.username = instance
            p.save()
    post_save.connect(user_post_save, sender=AbstractUser)


#    def save(self):
#    def save(self, **kwargs):
    def save(self, *args, **kwargs):
#        self.country = self.city.country.name
#        self.latitude = self.birthplace.latitude
#        self.longitude = self.birthplace.longitude
        if not self.slug:
            self.slug = slugify(self.username)
        if not self.pk:
            self.country
            self.latitude
            self.longitude
#            self.set_coords()
        super(Profile, self).save(*args, **kwargs)

#        for city in self.city_set.all():
#            city.country = "%s" % city.country

#        if not self.location:
#            return False
#        else:
#            lokasi = Profile.objects.values_list('location', flat=True)
#            self.latitude = lokasi[0].split(',')
#            self.longitude = lokasi[1].split(',')
#            self.location = self.latitude, self.longitude
#            self.location = str(location[0]) + ", " + str(location[1])
#            self.latitude = self.location[0]
#            self.longitude = self.location[1]
#        super(Profile, self).save()
#        super(Profile, self).save(**kwargs)
#        super(Profile, self).save(*args, **kwargs)


#class Aspect(models.Model):
#    aspect = models.CharField(verbose_name='Aspect', max_length=100, blank=False, null=True)
#    username = models.ForeignKey(Profile, related_name="%(app_label)s_%(class)s_username", verbose_name='Username', max_length=100, blank=False)
#    username = models.ManyToManyField(Profile, related_name="%(app_label)s_%(class)s_username", verbose_name='Username', max_length=100, blank=False)
#    birthdate = models.DateField(verbose_name='Birthdate', blank=False, null=True)
#    birthtime = models.TimeField(verbose_name='Birthtime', blank=True, null=True)
#    birthplace = models.CharField(verbose_name='Birthplace', max_length=100, blank=False, null=True)

#    class Meta:
#        db_table = 'aspect'
#        verbose_name = "Aspect"
#        verbose_name_plural = "Aspects"

#    def __str__(self):              # __unicode__ on Python 2
#        return self.aspect

#    @property
#    def zodiac(self):
#        tgldt = self.birthdate
#        tgl = arrow.get(tgldt).format('YYYY/MM/DD')
#        wkt = self.birthtime
#        waktu = wkt.strftime('%H:%M')
#        gmt = self.birthplace.gmt_offset
#        tglwkt = Datetime(tgl, waktu, gmt)
#        latitude = self.birthplace.latitude
#        longitude = self.birthplace.longitude
#        pos = GeoPos(latitude, longitude)

#        chart = Chart(tglwkt, pos)
#        sun = chart.get(const.SUN)
#        return sun.sign
