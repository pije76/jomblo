import autocomplete_light.shortcuts as al

from apps.accounts.models import Profile

al.register(Profile,
    search_fields=['^first_name', 'last_name'],
    attrs={
        'placeholder': 'Other model name ?',
        'data-autocomplete-minimum-characters': 1,
    },
    widget_attrs={
        'data-widget-maximum-values': 4,
        'class': 'modern-style',
    },
)
