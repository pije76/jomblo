from ajax_select import LookupChannel, register

from apps.geonames.models import City
from apps.accounts.models import Profile

class StandardLookupChannel(LookupChannel):
    def format_match(self, obj):
        return self.get_result(obj)

    def format_item_display(self, obj):
        return self.get_result(obj)


@register('city')
class CityLookup(StandardLookupChannel):
    model = City

    def check_auth(self, request):
        if request.user.is_anonymous:
            return True

    def get_query(self, q, request):
#        return City.objects.filter(name__icontains=q).select_related('country').distinct()
#        return City.objects.filter(name__icontains=q)
        return self.model.objects.filter(name__icontains=q)

    def format_item_display(self, item):
        return "<span class='city'>%s</span>" % item.name


@register('username')
class UsernameLookup(StandardLookupChannel):
    model = Profile

    def check_auth(self, request):
        if request.user.is_anonymous:
            return True

    def get_query(self, q, request):
#        return City.objects.filter(name__icontains=q).select_related('country').distinct()
#        return City.objects.filter(name__icontains=q)
        return self.model.objects.filter(username__icontains=q)

    def format_item_display(self, item):
        return "<span class='city'>%s</span>" % item.username
