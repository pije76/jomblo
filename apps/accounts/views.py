from django.contrib.auth import login, authenticate
from django.shortcuts import render, redirect, get_object_or_404
from django.http import HttpResponseRedirect
from django.views.generic import DetailView, ListView, TemplateView
from django.contrib.postgres.search import SearchVector
from django.db.models import Q
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from django.contrib.auth.decorators import login_required

import re
import arrow

from hitcount.views import HitCountDetailView
from hitcount.models import HitCount
from hitcount.views import HitCountMixin
from dal import autocomplete

from apps.flatlib import const
from apps.flatlib.chart import Chart
from apps.flatlib.datetime import Datetime
from apps.flatlib.geopos import GeoPos

from apps.accounts.filters import ProfileFilter
from apps.accounts.forms import SignUpForm, ProfileUpdate
from apps.accounts.models import Profile
from apps.geonames.models import City

# Create your views here.
def homepage(request):
    profiles = Profile.objects.all()
    return render(
        request, 'index.html',
        {
            'profiles': profiles,
            "basetitle": "Chiroon",
            "title": "Find your truly soulmate ",
        }
    )

def signupform(request):
    if request.method == 'POST':
        signupform = SignUpForm(request.POST)
        if signupform.is_valid():
            user = signupform.save()
            user.refresh_from_db()  # load the profile instance created by the signal
            user.profile.first_name = signupform.cleaned_data.get('first_name')
            user.profile.gender = signupform.cleaned_data.get('gender')
            user.profile.relationship = signupform.cleaned_data.get('relationship')
            user.profile.birthdate = signupform.cleaned_data.get('birthdate')
            user.profile.birthcity = signupform.cleaned_data.get('birthcity')
            user.profile.birthplace = signupform.cleaned_data.get('birthplace')
            user.profile.username = signupform.cleaned_data.get('username')
            user.profile.email = signupform.cleaned_data.get('email')
            user.save()
            raw_password = signupform.cleaned_data.get('password1')
            user = authenticate(username=user.username, password=raw_password)
            login(request, user)
            return redirect('homepage')
    else:
        signupform = SignUpForm()
    return render(request, 'account/signup.html', {'signupform': signupform})


#def profilelist(request):
#    profile = Profile.objects.all()
#    return render(
#        request, 'account/profilelist.html',
#        {
#            'profile': profile
#        }
#    )

class ProfileList(ListView):
    model = Profile
    template_name = 'account/profilelist.html'

    def get_context_data(self, **kwargs):
        context = super(ProfileList, self).get_context_data(**kwargs)
##        context['post_list'] = Profile.objects.all()[:5]
        context['profile_list'] = Profile.objects.all().order_by('hitcount__hits')
##        context['profile_views'] = ["profilelist"]
        return context

class ProfileDetail(HitCountDetailView):
    model = Profile
    count_hit = True
    template_name = 'account/profiledetail.html'

    def get_context_data(self, **kwargs):
        context = super(ProfileDetail, self).get_context_data(**kwargs)
##        context['post_list'] = Profile.objects.all()[:5]
##        context['profile_list'] = Profile.objects.all()
        context['profile_views'] = ["profiledetail"]
        return context

def profiledetail(request, slug=None):
    instance = get_object_or_404(Profile, slug=slug)
    if request.user.is_authenticated():
        return render(
            request, "account/profiledetail.html",
            {
                'detail_object': instance,
                "basetitle": "Chiroon",
                "title": instance.first_name,
            }
        )
    else:
        return render(
            request, "account/profiledetail.html",
            {
                'detail_object': instance,
                "basetitle": "Chiroon",
                "title": instance.first_name,
            }
        )

def profileupdate(request, profile_id):
    profile = Profile.objects.all()
    myflow = Profile.objects.filter(username=request.user)
    flow = get_object_or_404(Profile, pk=profile_id)
    form = SignUpForm(request.POST or None, instance=flow)
    if form.is_valid():
        editprofile = form.save(commit=False)
        editprofile.user = request.user
        editprofile.save()
        return HttpResponseRedirect(editprofile.get_absolute_url())
    return render(
        request, 'account/profileupdate.html',
        {
            'profile': profile,
            'myflow': myflow,
            'flow': flow,
            'form': form
        }
    )

def get_query(query_string, search_fields):

    query = None
    terms = re.compile(r'[^\s";,.:]+').findall(query_string)
    for term in terms:
        or_query = None
        for field_name in search_fields:
            q = Q(**{"%s__icontains" % field_name: term})
            if or_query is None:
                or_query = q
            else:
                or_query = or_query | q
        if query is None:
            query = or_query
        else:
            query = query & or_query
    return query

#class SeachMixin(object):
#    def get_context_data(self, **kwargs):
#        context = super(SeachMixin, self).get_context_data(**kwargs)
#        context['search_property_form'] = SearchForm()
#        return context

#class PropertyView(SeachMixin,DetailView):
#    template_name = 'properties/view.html'
#    context_object_name = 'house'
#    queryset = Profile.objects.select_related(...).prefetch_related(...).filter(flag_active=True, flag_status='a')

class Autocomplete(autocomplete.Select2QuerySetView):
    def get_queryset(self):
        qs = Profile.objects.all()

        if self.q:
            qs = qs.filter(username__icontains=self.q)

        return qs

#def searchform(request):
#    return render(
#        request, 'search/search.html',
#        {
#        }
#    )

def searchform(request):
    queryset = Profile.objects.all().order_by('-date_joined')

#    for object in queryset:
#        if object.pId == "TMG":
#            object.pId = "TMG00" + str(object.tmId)
#            object.save()

    query1 = request.GET.get('gender')
    query2 = request.GET.get('relationship')
    query3 = request.GET.get('min_age')
    query4 = request.GET.get('max_age')
    query5 = request.GET.get('religion')

    if query1 :#and query2 and query3 and query4 and query5 :
        queryset = queryset.filter(gender=query1)
        queryset = queryset.filter(relationship__icontains=query2)
        queryset = queryset.filter(age__gte=int(query3))
        queryset = queryset.filter(age__lte=int(query4))
        queryset = queryset.filter(religion__icontains=query5)#.filter( p_age_max__lte=query4)

    paginator = Paginator(queryset, 5)  # Show 5 contacts per page

    page = request.GET.get('page')
    try:
        queryset1 = paginator.page(page)
    except PageNotAnInteger:
        # If page is not an integer, deliver first page.
        queryset1 = paginator.page(1)
    except EmptyPage:
        # If page is out of range (e.g. 9999), deliver last page of results.
        queryset1 = paginator.page(paginator.num_pages)

    return render(
        request, "search/search.html",
        {
            "page_list": queryset1,
            "objectset": queryset1,
            "title": "list"
        }
    )


def searchresult(request):
#    query_string = ''
#    found_entries = None

#    if ('q' in request.GET) and request.GET['q'].strip():
#        query_string = request.GET['q']
#        entry_query = get_query(query_string, ['username', 'slug',])
        user_list = Profile.objects.all()
#        found_entries = Profile.objects.filter(username__icontains=query_string)
        found_entries = ProfileFilter(request.GET, queryset=user_list)
#        found_entries = Profile.objects.filter(entry_query).order_by('-date_joined')
#        found_entries = Profile.objects.annotate(search=SearchVector('username', 'slug',),).filter(search=query_string)

        return render(
            request, 'search/searchresult.html',
            {
#                'query_string': query_string,
                'found_entries': found_entries,
            }
        )

