from django import forms
from django.contrib.auth import get_user_model
from django.contrib.auth.forms import UserCreationForm
#from django.contrib.gis.geoip2 import GeoIP2
from django.forms import ModelForm
from django.forms.extras.widgets import SelectDateWidget

from datetime import datetime

from ajax_select.fields import AutoCompleteSelectField, AutoCompleteSelectMultipleField
#from allauth.account.forms import SetPasswordField, PasswordField

from apps.accounts.models import Profile
from apps.accounts.lookups import CityLookup, UsernameLookup
from apps.accounts.select_time_widget import SelectTimeWidget # See __all__.

date_range = 100
this_year = datetime.now().year

GENDER_CHOICES = (
    ('m', 'Male'),
    ('f', 'Female'),
)

RELATIONSHIP_CHOICES = (
    ('s', 'Single'),
    ('m', 'Married'),
    ('w', 'Widow/Widower'),
    ('d', 'Divorced'),
)

#class SignUpForm(UserCreationForm):
#class SignUpForm(ModelForm):
#class SignUpForm(forms.ModelForm):
class SignUpForm(forms.Form):
    first_name = forms.CharField(max_length=100, required=True)
#    gender = forms.TypedChoiceField(choices=GENDER_CHOICES, widget=forms.Select(attrs={'class': 'input-lg'}), required=True)
#    relationship = forms.TypedChoiceField(choices=RELATIONSHIP_CHOICES,widget=forms.Select(attrs={'class': 'input-lg'}),required=True)
#    birthdate = forms.DateField(widget=SelectDateWidget(years=list(range(this_year - date_range, this_year + date_range))))
#    birthtime = forms.TimeField(widget=SelectTimeWidget(twelve_hr=False))
#    birthplace = forms.ModelChoiceField(
#        queryset=Profile.objects.all(),
#        widget=autocomplete.ModelSelect2(url='country-autocomplete')
#    )
#    birthplace = forms.CharField()
    birthplace = AutoCompleteSelectField('city', required=True, help_text=None)
#    location = PlainLocationField(based_fields=['birthplace'], zoom=7, initial=None)

#    username = forms.CharField(max_length=100, required=True)
#    email = forms.EmailField(required=True)
#    password1 = SetPasswordField()
#    password2 = PasswordField()

    def signup(self, request, user):
        user.first_name = self.cleaned_data['first_name']
        user.birthplace = self.cleaned_data['birthplace']
        user.save()

    class Meta:
        model = get_user_model() # use this function for swapping user model



#    def save(self):
#        new_user = super(SignUpForm, self).save()

#        profile = new_user.get_profile()
#        profile.mugshot = self.cleaned_data['avatar']
#        profile.save()

#        return new_user

class ProfileUpdate(forms.Form):
    slug = forms.CharField(max_length=100)
#    username = AutoCompleteSelectField('username', required=False)

    class Meta:
        model = get_user_model()
