from django import forms
from django.utils.safestring import mark_safe
from datetime import datetime, date, time, timedelta


class TimePickerWidget(forms.TimeInput):
    def render(self, name, value, attrs=None):
        times = []
        htmlString = u''
        htmlString += u'<select name="%s">' % (name)
        for i in range(24):
                htmlString += ('<option value="%d:00">%d:00</option>' % (i,i))
        htmlString +='</select>'
        return mark_safe(u''.join(htmlString))
