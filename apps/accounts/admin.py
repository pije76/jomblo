# -*- coding: utf-8 -*-


from django.contrib import admin

from .models import Profile

# Register your models here.
class ProfileAdmin(admin.ModelAdmin):
    list_display = (
        'username',
        'slug',
        'email',
#        'hitcount',
        'first_name',
        'address',
        'city',
#        'state',
        'country',
        'postalcode',
        'relationship',
        'gender',
        'birthplace',
#        'location',
        'latitude',
        'longitude',
        'gmtoffset',
        'birthdate',
        'birthtime',
        'zodiac',
        'age'
    )
    search_fields = ['username']
    raw_id_fields = ('city', 'birthplace',)
    list_per_page = 25


admin.site.register(Profile, ProfileAdmin)
#admin.site.register(Aspect)
