from django.contrib import admin

from .models import Aspect

# Register your models here.
class AspectAdmin(admin.ModelAdmin):
    list_display = (
        'aspect',
#        'age'
    )
    search_fields = ['aspect']
    list_per_page = 25


admin.site.register(Aspect, AspectAdmin)
