#!/usr/bin/env python2.7
import ephem
from bisect import bisect
from math import radians

class NotFound(Exception):
    pass

def find_altitude(altitudes, body, observer, start, stop, step):
    """Find the first altitude crossed by a body in a time period.

    Parameters
    altitudes: list(float) -- the target altitudes in degrees
    body: Body -- the body
    observer: Observer -- observer from whose location the altitude is calculated.
    start: datetime -- start of the period to be searched
    stop: datetime -- end of the period to be searched
    step: timedelta -- size of the search steps

    Returns a pair (altitude, datetime) whose first element is the first altitude crossed by the body in the time period,
    and whose second element is the datetime at which that happens.

    If none of the altitudes is crossed in the time period, raise NotFound.

    """
    orig_altitudes = sorted(altitudes)

    # Convert inputs to PyEphem data structures.
    altitudes = list(map(radians, orig_altitudes))
    date = ephem.Date(start)
    stop = ephem.Date(stop)
    step = step.total_seconds() * ephem.second

    def alt(d):
        # Return altitude of body as seen by observer at date d.
        observer.date = d
        body.compute(observer)
        return body.alt

    # Position of alt(date) in sorted list of altitudes.
    pos = bisect(altitudes, alt(date))
    while True:
        # Advance current date forward by one step.
        old_date = date
        date += step
        if date > stop:
            raise NotFound()

        old_pos = pos
        pos = bisect(altitudes, alt(date))
        if old_pos != pos:
            # At least one of the target altitudes was crossed.
            break

    # Find the index i of the first target altitude that was crossed.
    if old_pos < pos:       # Moving forward in list.
        i = old_pos
    else:                   # Moving backward in list.
        i = old_pos - 1

    # Solve alt(d) == altitudes[i] using Newton's method.
    def f(d):
        return alt(d) - altitudes[i]
    crossing_date = ephem.newton(f, old_date, date)

    # Convert outputs back again.
    return orig_altitudes[i], ephem.Date(crossing_date).datetime()
