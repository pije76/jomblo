from django.db import models

from apps.flatlib import aspects
from apps.flatlib import const
from apps.flatlib.chart import Chart
from apps.flatlib.datetime import Datetime
from apps.flatlib.geopos import GeoPos

import arrow

from apps.accounts.models import Profile

ASPECT_CHOICES = (
    ('No_Aspect', 'No Aspect'),
    ('Conjuction', 'Conjuction'),
    ('Sextile', 'Sextile'),
    ('Square', 'Square'),
    ('Trine', 'Trine'),
    ('Opposition', 'Opposition'),
)

# Create your models here.
class Aspect(models.Model):
    aspect = models.CharField(verbose_name='Aspect', max_length=100, blank=False, null=True)
#    username = models.ForeignKey(Profile, related_name="%(app_label)s_%(class)s_username", verbose_name='Username', max_length=100, blank=False)
    username = models.ManyToManyField(Profile, related_name="%(app_label)s_%(class)s_username", verbose_name='Username', max_length=100, blank=False)
    birthdate = models.DateField(verbose_name='Birthdate', blank=False, null=True)
    birthtime = models.TimeField(verbose_name='Birthtime', blank=True, null=True)
    birthplace = models.CharField(verbose_name='Birthplace', max_length=100, blank=False, null=True)

    class Meta:
        db_table = 'aspect'
        verbose_name = "Aspect"
        verbose_name_plural = "Aspects"

    def __str__(self):              # __unicode__ on Python 2
        return self.aspect

    @property
    def zodiac(self):
        tgldt = self.birthdate
        tgl = arrow.get(tgldt).format('YYYY/MM/DD')
        wkt = self.birthtime
        waktu = wkt.strftime('%H:%M')
        gmt = self.birthplace.gmt_offset
        tglwkt = Datetime(tgl, waktu, gmt)
        latitude = self.birthplace.latitude
        longitude = self.birthplace.longitude
        pos = GeoPos(latitude, longitude)

        chart = Chart(tglwkt, pos)
        sun = chart.get(const.SUN)
        return sun.sign
