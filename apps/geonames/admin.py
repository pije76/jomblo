from django.contrib import admin

from .models import City, Country

class CityAdmin(admin.ModelAdmin):
    list_display = (
        'name',
        'latitude',
        'longitude',
        'timezone',
        'gmt_offset',
        'country',
    )
    search_fields = ['name',]
    list_per_page = 25

class CountryAdmin(admin.ModelAdmin):
    list_display = (
        'name',
        'capital',
        'iso',
        'fips',
    )
    search_fields = ['name',]
    list_per_page = 25

admin.site.register(Country,CountryAdmin)
admin.site.register(City,CityAdmin)
